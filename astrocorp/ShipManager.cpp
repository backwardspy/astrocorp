#include "ShipManager.h"
#include "Ship.h"

void ShipManager::add(Ship *ship)
{
	m_ships.push_back(std::unique_ptr<Ship>(ship));
}

void ShipManager::remove(Ship *ship)
{
	m_ships.remove_if([&](std::unique_ptr<Ship> &ptr){ return ptr.get() == ship; });
}

void ShipManager::input(const sf::Event &event)
{
	for (auto &ship : m_ships) ship->input(event);
}

void ShipManager::update(float dt)
{
	// Move all ships.
	for (auto &ship : m_ships) ship->update(dt);

	// Resolve collisions.
	for (auto it1 = m_ships.begin(); it1 != m_ships.end(); ++it1)
	{
		Ship *a = (*it1).get();
		for (auto it2 = m_ships.begin(); it2 != it1; ++it2)
		{
			Ship *b = (*it2).get();

			Vec2 diff = b->position - a->position;

			const static float MIN_DIST = 2000.0f;

			if (diff.lengthSq() <= MIN_DIST)
			{
				Vec2 force = diff.normalised() * (MIN_DIST - diff.lengthSq()) * 0.01f;
				a->addForce(-force);
				b->addForce(force);
			}
		}
	}
}

void ShipManager::draw(sf::RenderTarget &context)
{
	for (auto &ship : m_ships) ship->draw(context);
}