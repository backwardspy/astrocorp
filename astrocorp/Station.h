#pragma once

#include <sstream>
#include "ClickableObject.h"

class Station : public ClickableObject
{
public:
	Station();
	~Station();
	void update(float dt);
	void draw(sf::RenderTarget &context);

	int ore;

private:
	std::stringstream m_sstream;
	sf::Text m_oreText;
};