#include "Game.h"
#include "MainMenuGUI.h"
#include "Resources.h"

void MainMenuGUI::init()
{
	sf::Vector2u screen = game.getWindowSize();

	auto *p = new GUIPanel();
	p->w = 300.0f;
	p->h = 220.0f;
	p->x = (screen.x - p->w) / 2.0f;
	p->y = (screen.y - p->h) / 2.0f;

	auto *label = new GUILabel();
	label->x = p->x;
	label->y = p->y;
	label->w = p->w;
	label->h = 64.0f;
	label->text = "AstrOS v7.4";

	auto *startButton = new GUIButton(), *exitButton = new GUIButton();

	startButton->x = p->x + 12.0f;
	startButton->y = label->y + label->h + 8.0f;
	startButton->w = p->w - 24.0f;
	startButton->h = 64.0f;
	startButton->text = "Start";
	startButton->setCallback(m_startCallback);

	exitButton->x = p->x + 12.0f;
	exitButton->y = startButton->y + startButton->h + 8.0f;
	exitButton->w = p->w - 24.0f;
	exitButton->h = 64.0f;
	exitButton->text = "Exit";
	exitButton->setCallback(m_exitCallback);

	m_controls.push_back(std::unique_ptr<GUIPanel>(p));
	m_controls.push_back(std::unique_ptr<GUILabel>(label));
	m_controls.push_back(std::unique_ptr<GUIButton>(startButton));
	m_controls.push_back(std::unique_ptr<GUIButton>(exitButton));

	sf::Texture *logoImage = resources.get<sf::Texture>("sprites/logo");

	if (logoImage)
	{
		GUIImage *logo = new GUIImage();
		logo->image = sf::Sprite(*logoImage);
		logo->w = (float)logoImage->getSize().x;
		logo->h = (float)logoImage->getSize().y;
		logo->x = (screen.x - logo->w) / 2.0f;
		logo->y = screen.y / 4.0f - logo->h / 2.0f;

		m_controls.push_back(std::unique_ptr<GUIImage>(logo));
	}
}