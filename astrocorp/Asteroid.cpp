#include "Asteroid.h"
#include "Math.h"

Asteroid::Asteroid():
	ClickableObject("sprites/objects/asteroid"),
	ore(Math::rand(30000, 80000)),
	m_rotSpeed(Math::rand(-0.5f, 0.5f))
{

}

Asteroid::~Asteroid()
{

}

void Asteroid::update(float dt)
{
	rotation += m_rotSpeed * dt;
}