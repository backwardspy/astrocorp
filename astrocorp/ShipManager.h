#pragma once

#include <list>
#include <memory>
#include <SFML/Graphics.hpp>

class Ship;

class ShipManager
{
public:
	void add(Ship *ship);
	void remove(Ship *ship);

	void input(const sf::Event &event);
	void update(float dt);
	void draw(sf::RenderTarget &context);

private:
	std::list<std::unique_ptr<Ship>> m_ships;
};