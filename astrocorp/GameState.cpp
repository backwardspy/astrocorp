#include "AIShip.h"
#include "MiningShip.h"
#include "GameState.h"
#include "Game.h"
#include "Resources.h"

GameState::GameState():
	State(true, true),
	m_money(500)
{
	m_player = new PlayerShip();
	m_player->position = Vec2(game.getWindowSize().x / 2.0f, game.getWindowSize().y / 2.0f);

	m_shipManager.add(m_player);

	for (int i = 0; i < 1; i++)
	{
		AIShip *ai = new AIShip("sprites/ships/pirate");
		ai->position = Vec2(Math::rand((float)game.getWindowSize().x), Math::rand((float)game.getWindowSize().y));

		m_shipManager.add(ai);
	}

	for (int i = 0; i < 5; i++)
	{
		Asteroid ast;
		ast.setCallback([&](ClickableObject &object) { m_player->mine((Asteroid *)&object); });
		ast.position = Vec2(400.0f, 400.0f) + Vec2(Math::rand(-200.0f, 200.0f), Math::rand(-200.0f, 200.0f));
		m_asteroids.push_back(ast);
	}

	m_testStation.setCallback([&](ClickableObject &object)
	{
		m_player->flyTo(m_testStation, [&]
		{
			m_stationGUI.show();
		});
	});
	m_testStation.position = Vec2((float)game.getWindowSize().x - 200.0f, (float)game.getWindowSize().y - 200.0f);

	m_stationGUI.setSellCallback([&]
	{
		m_money += m_testStation.ore * 2;
		m_testStation.ore = 0;
	});
	m_stationGUI.setBuyShipCallback([&]
	{
		if (m_money < 500) return;

		m_money -= 500;

		MiningShip *miner = new MiningShip;
		miner->position = m_testStation.position + Math::randVec() * 50.0f;
		miner->setDropOff(&m_testStation);

		m_shipManager.add(miner);
		m_player->addMiner(miner);
	});
	m_stationGUI.init();
	m_stationGUI.hide();

	auto *font = resources.get<sf::Font>("fonts/neuropol x free");
	if (font)
	{
		m_moneyText.setFont(*font);
		m_moneyText.setCharacterSize(24);
		m_moneyText.setPosition(12.0f, 12.0f);
		m_moneyText.setColor(sf::Color::White);
	}
}

GameState::~GameState()
{
}

void GameState::input(const sf::Event &event)
{
	if (!m_stationGUI.active())
	{
		m_shipManager.input(event);
		m_testStation.input(event);

		for (auto &rock : m_asteroids) rock.input(event);
	}

	m_stationGUI.input(event);
}

void GameState::update(float dt)
{
	State::update(dt);

	m_shipManager.update(dt);
	m_testStation.update(dt);

	for (auto &rock : m_asteroids) rock.update(dt);

	m_stationGUI.update(dt);
}

void GameState::draw(sf::RenderTarget &context)
{
	m_testStation.draw(context);

	for (auto &rock : m_asteroids) rock.draw(context);

	m_shipManager.draw(context);

	m_stationGUI.draw(context);

	m_sstream << "Money: " << m_money << " Cr";
	m_moneyText.setString(m_sstream.str());
	context.draw(m_moneyText);
	m_sstream.str("");

	State::draw(context);
}