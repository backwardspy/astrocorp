#include "Game.h"
#include "GameState.h"
#include "MainMenuState.h"
#include "StateManager.h"
#include "Resources.h"

MainMenuState::MainMenuState() : State(true, true)
{
	m_gui.setStartCallback([&]{ exit([&]{ m_manager->changeTo(new GameState()); }); });
	m_gui.setExitCallback([&] { exit(std::bind(&Game::exit, &game)); });

	m_gui.init();

	if (m_music = resources.get<sf::Music>("audio/spacemusic"))
	{
		m_music->setLoop(true);
		m_music->setVolume(0.0f);
		m_music->play();
	}
}

MainMenuState::~MainMenuState()
{

}

void MainMenuState::input(const sf::Event &event)
{
	m_gui.input(event);
}

void MainMenuState::update(float dt)
{
	State::update(dt);

	switch (m_stage)
	{
	case Normal:
		m_gui.update(dt);
		break;

	case Entering:
		if (m_music) m_music->setVolume((m_transitionTimer / TRANSITION_TIME) * 60.0f);
		break;

	case Exiting:
		if (m_music) m_music->setVolume((1.0f - (m_transitionTimer / TRANSITION_TIME)) * 60.0f);
		break;
	}
}

void MainMenuState::draw(sf::RenderTarget &context)
{
	m_gui.draw(context);

	State::draw(context);
}