#pragma once

#include <SFML/Graphics.hpp>

class Vec2
{
public:
	static Vec2 zero;
	static Vec2 up;
	static Vec2 right;

	float x, y;

	Vec2();
	Vec2(float v);
	Vec2(float x, float y);

	float lengthSq() const;
	float length() const;
	void normalise();
	Vec2 normalised() const;
	float dot(const Vec2 &other);

	Vec2 operator +(const Vec2 &other) const;
	Vec2 operator +=(const Vec2 &other);
	Vec2 operator -(const Vec2 &other) const;
	Vec2 operator -=(const Vec2 &other);
	Vec2 operator *(float other) const;
	Vec2 operator *=(float other);
	Vec2 operator /(float other) const;
	Vec2 operator -() const;

	operator sf::Vector2f() const;
};

float operator *(const Vec2 &a, const Vec2 &b);

namespace Math
{
	const float PI = 3.14159265f;
	const float DEG2RAD = PI / 180.0f;
	const float RAD2DEG = 180.0f / PI;

	template<class T>
	T min(T a, T b) { return a < b ? a : b; }

	template<class T>
	T max(T a, T b) { return a > b ? a : b; }

	template<class T>
	T wrapAngle(T angle)
	{
		angle = fmod(angle + PI, PI * 2.0f);
		if (angle < 0.0f) angle += PI * 2.0f;
		return angle - PI;
	}

	template<class T>
	T rand(T max) { return T((std::rand() / (double)RAND_MAX) * max); }

	template<class T>
	T rand(T min, T max) { return min + T((std::rand() / (double)RAND_MAX) * (max - min)); }

	Vec2 randVec();
}