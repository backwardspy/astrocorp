#pragma once

#include <list>
#include <SFML/Graphics.hpp>
#include "Particle.h"

class ParticleEmitter
{
public:
	ParticleEmitter();
	~ParticleEmitter();

	void add(Particle p) { m_particles.push_back(p); }

	void update(float dt);
	void draw(sf::RenderTarget &context);

private:
	std::list<Particle> m_particles;

	sf::Sprite m_sprites[Particle::TypeCount];
};