#include "Ship.h"
#include "Resources.h"
#include "Math.h"
#include "ClickableObject.h"

Ship::Ship(float speed, const std::string &sprite):
	rotation(0.0f), m_rotVel(0.0f),
	m_autopilot(false),
	m_targetObject(nullptr),
	m_speed(speed)
{
	auto tex = resources.get<sf::Texture>(sprite);

	sf::Vector2f origin(0.0f, 0.0f);

	if (tex)
	{
		origin = sf::Vector2f(tex->getSize().x / 2.0f, tex->getSize().y / 2.0f);

		m_sprite.setTexture(*tex);
		m_sprite.setOrigin(origin);
	}

	// Load each thruster.
	if (tex = resources.get<sf::Texture>(sprite + "_mainThrust"))
	{
		m_thrusterSprites[Main].setTexture(*tex);
		m_thrusterSprites[Main].setOrigin(origin);
	}

	if (tex = resources.get<sf::Texture>(sprite + "_leftThrust"))
	{
		m_thrusterSprites[Left].setTexture(*tex);
		m_thrusterSprites[Left].setOrigin(origin);
	}

	if (tex = resources.get<sf::Texture>(sprite + "_rightThrust"))
	{
		m_thrusterSprites[Right].setTexture(*tex);
		m_thrusterSprites[Right].setOrigin(origin);
	}

	for (auto &state : m_thrusterStates) state = false;

	if (tex = resources.get<sf::Texture>("sprites/effects/laser"))
	{
		m_laserSprite.setTexture(*tex);
		m_laserSprite.setOrigin(0.0f, tex->getSize().y / 2.0f);
	}

	if (tex = resources.get<sf::Texture>("sprites/particles/glowSmall"))
	{
		m_laserGlowSprite.setTexture(*tex);
		m_laserGlowSprite.setOrigin(tex->getSize().x / 2.0f, tex->getSize().y / 2.0f);
	}
}

Ship::~Ship()
{

}

void Ship::update(float dt)
{
	if (m_autopilot)
	{
		Vec2 destination;
		if (m_targetObject) destination = m_targetObject->position;
		else destination = m_targetPos;
		flyTo(destination);

		if (m_targetObject && m_targetCallback)
		{
			if ((m_targetObject->position - position).lengthSq() <= m_targetObject->clickRadius * m_targetObject->clickRadius)
			{
				m_targetCallback();

				m_targetObject = nullptr;
				m_targetCallback = nullptr;
				m_autopilot = false;	// TODO: Waypoints?
			}
		}
	}

	position += m_velocity * dt;
	m_velocity *= powf(0.5f, dt);
	m_velocity += m_force;
	m_force = Vec2::zero;

	rotation += m_rotVel * dt;
	m_rotVel *= powf(0.999f, dt);

	Vec2 forward(cosf(rotation), sinf(rotation));
	Vec2 sideways(-forward.y, forward.x);
	float sideslip = m_velocity.dot(sideways);

	m_velocity -= sideways * sideslip * dt;

	if (abs(sideslip) > 5.0f)
	{
		if (sideslip > 0.0f)
		{
			m_thrusterStates[Left] = false;
			m_thrusterStates[Right] = true;
		}
		else
		{
			m_thrusterStates[Left] = true;
			m_thrusterStates[Right] = false;
		}
	}

	// Update lasers.
	auto &laser = m_lasers.begin();
	while (laser != m_lasers.end())
	{
		laser->lifetime -= dt;
		if (laser->lifetime <= 0.0f) laser = m_lasers.erase(laser);
		else ++laser;
	}
}

void Ship::draw(sf::RenderTarget &context)
{
	for (auto laser : m_lasers)
	{
		m_laserSprite.setPosition(laser.position);
		m_laserSprite.setRotation(laser.rotation * Math::RAD2DEG);
		m_laserSprite.setScale(laser.length, laser.size - (1.0f - laser.lifetime / laser.maxLife) * laser.size);
		m_laserSprite.setColor(laser.colour);
		context.draw(m_laserSprite);

		m_laserGlowSprite.setPosition(laser.endPoint);
		float scale = (laser.size - (1.0f - laser.lifetime / laser.maxLife) * laser.size) * 3.0f;
		m_laserGlowSprite.setScale(scale, scale);
		m_laserGlowSprite.setColor(laser.colour);
		context.draw(m_laserGlowSprite);
	}

	m_sprite.setPosition(position);
	m_sprite.setRotation(rotation * Math::RAD2DEG);
	context.draw(m_sprite);

	for (int i = 0; i < 3; i++)
	{
		if (m_thrusterStates[i])
		{
			m_thrusterSprites[i].setPosition(position);
			m_thrusterSprites[i].setRotation(rotation * Math::RAD2DEG);
			context.draw(m_thrusterSprites[i]);

			m_thrusterStates[i] = false;
		}
	}
}

void Ship::thrust(float power)
{
	if (power == 0.0f) return;

	if (power < -0.2f) power = -0.2f;
	if (power > 1.2f) power = 1.0f;
	m_thrusterStates[Main] = true;
	Vec2 direction(cosf(rotation), sinf(rotation));
	m_velocity += direction.normalised() * m_speed * power;
}

void Ship::rotate(float targetAngle)
{
	m_rotVel = Math::wrapAngle(targetAngle - rotation) * 10.0f;
	if (m_rotVel > 3.0f) m_rotVel = 3.0f;
	if (m_rotVel < -3.0f) m_rotVel = -3.0f;
}

void Ship::addForce(const Vec2 &force)
{
	m_force += force;
}

void Ship::flyTo(ClickableObject &object, std::function<void()> callback)
{
	m_autopilot = true;
	m_targetPos = object.position;
	m_targetObject = &object;
	m_targetCallback = callback;
}

void Ship::flyTo(const Vec2 &destination)
{
	Vec2 diff = destination - this->position;

	rotate(atan2(diff.y, diff.x));

	float targetSpeed = diff.length();
	float speedDiff = targetSpeed - m_velocity.length();
	float power = speedDiff * 10.0f;

	if (power < 0.0f) power = 0.0f;
	if (power > 1.0f) power = 1.0f;

	thrust(power);
}

void Ship::fireLaser(const sf::Color &colour, float life, float size, float length)
{
	m_lasers.push_back({ position, rotation, colour, life, life, size, length, position + Vec2(cos(rotation), sin(rotation)) * length });
}