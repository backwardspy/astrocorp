#pragma once

#include "GUIPanel.h"

class GUILabel : public GUIPanel
{
public:
	GUILabel();
	virtual ~GUILabel() { }

	void draw(sf::RenderTarget &context);
	
	std::string text;

protected:
	sf::Text m_text;
	sf::Font *m_font;
};