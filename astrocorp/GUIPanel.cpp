#include "GUIPanel.h"
#include "Resources.h"

GUIPanel::GUIPanel()
{
	// Set up the ninepatch.
	auto tex = resources.get<sf::Texture>("sprites/gui/ninepatch");
	if (tex == nullptr) return;

	m_tileSize = tex->getSize().x / 3;

	sf::Sprite sprite(*tex);

	for (int x = 0; x < 3; x++)
	{
		for (int y = 0; y < 3; y++)
		{
			m_patches[x][y] = sprite;
			m_patches[x][y].setTextureRect(sf::IntRect(x * m_tileSize, y * m_tileSize, m_tileSize, m_tileSize));
		}
	}
}

void GUIPanel::draw(sf::RenderTarget &context)
{
	sf::Sprite sprite;

	if (w < m_tileSize * 2.0f) w = m_tileSize * 2.0f;
	if (h < m_tileSize * 2.0f) h = m_tileSize * 2.0f;

	// Draw the corners.
	sprite = m_patches[0][0];	// Top left.
	sprite.setPosition(x, y);
	context.draw(sprite);

	sprite = m_patches[2][0];	// Top right.
	sprite.setPosition(x + w - m_tileSize , y);
	context.draw(sprite);

	sprite = m_patches[0][2];	// Bottom left.
	sprite.setPosition(x, y + h - m_tileSize);
	context.draw(sprite);

	sprite = m_patches[2][2];	// Bottom right.
	sprite.setPosition(x + w - m_tileSize, y + h - m_tileSize);
	context.draw(sprite);

	// Draw the horizontal edges.
	float edgeWidth = w - m_tileSize * 2.0f;
	if (edgeWidth > 0)
	{
		float scaleFactor = edgeWidth / m_tileSize;

		sprite = m_patches[1][0];	// Top.
		sprite.setPosition(x + m_tileSize, y);
		sprite.setScale(scaleFactor, 1.0f);
		context.draw(sprite);

		sprite = m_patches[1][2];	// Bottom.
		sprite.setPosition(x + m_tileSize, y + h - m_tileSize);
		sprite.setScale(scaleFactor, 1.0f);
		context.draw(sprite);
	}

	// Draw the vertical edges.
	float edgeHeight = h - m_tileSize * 2.0f;
	if (edgeHeight > 0)
	{
		float scaleFactor = edgeHeight / m_tileSize;

		sprite = m_patches[0][1];	// Left.
		sprite.setPosition(x, y + m_tileSize);
		sprite.setScale(1.0f, scaleFactor);
		context.draw(sprite);

		sprite = m_patches[2][1];	// Right.
		sprite.setPosition(x + w - m_tileSize, y + m_tileSize);
		sprite.setScale(1.0f, scaleFactor);
		context.draw(sprite);
	}

	// Draw the middle.
	if (edgeWidth > 0 && edgeHeight > 0)
	{
		float scaleX = edgeWidth / m_tileSize, scaleY = edgeHeight / m_tileSize;
		sprite = m_patches[1][1];
		sprite.setPosition(x + m_tileSize, y + m_tileSize);
		sprite.setScale(scaleX, scaleY);
		context.draw(sprite);
	}
}