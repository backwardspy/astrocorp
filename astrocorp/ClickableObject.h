#pragma once

#include <functional>
#include <SFML/Graphics.hpp>
#include "Math.h"

class ClickableObject
{
public:
	float clickRadius;

	ClickableObject(const std::string &sprite);
	virtual ~ClickableObject();

	virtual void input(const sf::Event &event);
	virtual void update(float dt) = 0;
	virtual void draw(sf::RenderTarget &context);

	void setCallback(std::function<void(ClickableObject &object)> callback) { m_callback = callback; }

	Vec2 position;
	float rotation;

protected:
	sf::Sprite m_sprite;
	std::function<void(ClickableObject &object)> m_callback;
};