#pragma once

#include <sstream>
#include "Ship.h"
#include "ParticleEmitter.h"

class Asteroid;
class Station;

class MiningShip : public Ship
{
public:
	MiningShip();
	~MiningShip();

	void mine(Asteroid *asteroid);
	void setDropOff(Station *station) { m_dropOff = station; }

	bool hasAsteroid() { return m_asteroid != nullptr; }

	void update(float dt);
	void draw(sf::RenderTarget &context);

private:
	void generateSitOffset();

	ParticleEmitter m_emitter;

	Asteroid *m_asteroid;
	Station *m_dropOff;

	int m_capacity, m_cargoAmount;

	float m_timer, m_digTimer;

	Vec2 m_sitOffset;

	std::stringstream m_sstream;
	sf::Text m_cargoText;
};