#pragma once

#include "State.h"
#include "MainMenuGUI.h"

class MainMenuState : public State
{
public:
	MainMenuState();
	~MainMenuState();

	void input(const sf::Event &event);
	void update(float dt);
	void draw(sf::RenderTarget &context);

private:
	MainMenuGUI m_gui;

	sf::Music *m_music;
};