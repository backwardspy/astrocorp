#pragma once

#include <list>
#include <memory>
#include <SFML/Graphics.hpp>

class State;

class StateManager
{
public:
	StateManager();
	~StateManager();

	void add(State *state);
	void remove(State *state);
	void changeTo(State *state);

	void input(const sf::Event &event);
	void update(float dt);
	void draw(sf::RenderTarget &target);

private:
	typedef std::list<std::unique_ptr<State>> StateList;
	StateList m_states;

	std::list<State *> m_removalList, m_additionList;
};