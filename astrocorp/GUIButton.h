#pragma once

#include <functional>
#include <SFML/Audio.hpp>
#include "GUILabel.h"

class GUIButton : public GUILabel
{
public:
	enum State { Normal, Hover };
	State getState() { return m_state; }

	GUIButton();
	virtual ~GUIButton() { }

	void input(const sf::Event &event);
	void draw(sf::RenderTarget &context);

	void setCallback(std::function<void()> callback) { m_callback = callback; }

private:
	bool inBounds(int x, int y);

	State m_state;

	std::function<void()> m_callback;

	sf::Sound m_beep, m_boop;
};