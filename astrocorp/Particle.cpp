#include "Particle.h"

Particle::Particle(const Vec2 &position, const Vec2 &velocity,
	const sf::Color &startColour, const sf::Color &endColour,
	float lifetime, Type type):
	position(position), velocity(velocity),
	startColour(startColour), endColour(endColour),
	lifetime(lifetime), maxLife(lifetime),
	type(type)
{

}