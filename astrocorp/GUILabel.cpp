#include "GUILabel.h"
#include "Resources.h"

GUILabel::GUILabel()
{
	GUIPanel::GUIPanel();

	text = "Label";

	m_text.setFont(*resources.get<sf::Font>("fonts/neuropol x free"));
	m_text.setCharacterSize(24);
}

void GUILabel::draw(sf::RenderTarget &context)
{
	GUIPanel::draw(context);

	m_text.setString(text);
	
	sf::FloatRect bounds = m_text.getGlobalBounds();

	float textX = x + (w - bounds.width) / 2.0f;
	float textY = y + (h - (bounds.height)) / 2.0f - 8.0f;

	m_text.setPosition(textX, textY);
	context.draw(m_text);
}