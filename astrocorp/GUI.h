#pragma once

#include <list>
#include <memory>
#include "GUIPanel.h"
#include "GUILabel.h"
#include "GUIButton.h"
#include "GUIImage.h"

class GUI
{
public:
	GUI();
	virtual ~GUI();

	virtual void init() = 0;

	void input(const sf::Event &event);
	void update(float dt);
	void draw(sf::RenderTarget &context);

	void show() { m_active = true; }
	void hide() { m_active = false; }

	bool active() const { return m_active; }

protected:
	std::list<std::shared_ptr<GUIPanel>> m_controls;
	bool m_active;
};