#include "GUIPanel.h"

class GUIImage : public GUIPanel
{
public:
	sf::Sprite image;

	void draw(sf::RenderTarget &context);

private:
};