#pragma once

#include "Ship.h"

class AIShip : public Ship
{
public:
	AIShip(const std::string &sprite);
	virtual ~AIShip();

	void input(const sf::Event &event);
	void update(float dt);
	void draw(sf::RenderTarget & context);
};