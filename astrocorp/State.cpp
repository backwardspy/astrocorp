#include "Game.h"
#include "State.h"

State::State(bool blocksUpdate, bool blocksDraw) :
	m_blocksUpdate(blocksUpdate), m_blocksDraw(blocksDraw),
	m_transitionTimer(0.0f)
{
	m_stage = Normal;

	m_fadeRect.setSize(sf::Vector2f(float(game.getWindowSize().x), float(game.getWindowSize().y)));
	m_fadeRect.setFillColor(sf::Color::Transparent);

	enter();
}

void State::update(float dt)
{
	switch (m_stage)
	{
	case Entering:
		if (m_transitionTimer >= TRANSITION_TIME) m_stage = Normal;

		m_transitionTimer += dt;
		if (m_transitionTimer > TRANSITION_TIME) m_transitionTimer = TRANSITION_TIME;
		break;

	case Exiting:
		if (m_transitionTimer >= TRANSITION_TIME)
		{
			if (m_exitFunc) m_exitFunc();
		}

		m_transitionTimer += dt;
		if (m_transitionTimer > TRANSITION_TIME) m_transitionTimer = TRANSITION_TIME;
		break;
	}
}

void State::draw(sf::RenderTarget &context)
{
	switch (m_stage)
	{
	case Entering:
		m_fadeRect.setFillColor(sf::Color(0, 0, 0, 255 - sf::Uint8((m_transitionTimer / TRANSITION_TIME) * 255.0f)));
		context.draw(m_fadeRect);
		break;

	case Exiting:
		m_fadeRect.setFillColor(sf::Color(0, 0, 0, sf::Uint8((m_transitionTimer / TRANSITION_TIME) * 255.0f)));
		context.draw(m_fadeRect);
		break;
	}
}

void State::enter()
{
	m_transitionTimer = 0.0f;
	m_stage = Entering;
}

void State::exit(std::function<void()> callback)
{
	m_transitionTimer = 0.0f;
	m_exitFunc = callback;
	m_stage = Exiting;
}