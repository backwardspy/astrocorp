#pragma once

#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

class Resources
{
public:
	~Resources();

	template<class T>
	T *get(const std::string &name);

private:
	template<class T> bool tryLoad(const std::string &name);
	template<> bool tryLoad<sf::Texture>(const std::string &name);
	template<> bool tryLoad<sf::Font>(const std::string &name);
	template<> bool tryLoad<sf::SoundBuffer>(const std::string &name);
	template<> bool tryLoad<sf::Music>(const std::string &name);

	std::map<std::string, void *> m_resources;
};

extern Resources resources;

template<class T>
T *Resources::get(const std::string &name)
{
	if (m_resources.find(name) == m_resources.end()) if (!tryLoad<T>(name)) return nullptr;
	return (T *)m_resources[name];
}

template<>
bool Resources::tryLoad<sf::Texture>(const std::string &name)
{
	sf::Texture *tex = new sf::Texture;
	
	if (!tex->loadFromFile("res/" + name + ".png")) return false;

	tex->setSmooth(true);
	m_resources[name] = (void *)tex;
	
	return true;
}

template<>
bool Resources::tryLoad<sf::Font>(const std::string &name)
{
	sf::Font *font = new sf::Font;

	if (!font->loadFromFile("res/" + name + ".ttf")) return false;

	m_resources[name] = (void *)font;

	return true;
}

template<>
bool Resources::tryLoad<sf::SoundBuffer>(const std::string &name)
{
	sf::SoundBuffer *buffer = new sf::SoundBuffer;

	if (!buffer->loadFromFile("res/" + name + ".ogg")) return false;

	m_resources[name] = (void *)buffer;

	return true;
}

template<> bool Resources::tryLoad<sf::Music>(const std::string &name)
{
	sf::Music *music = new sf::Music;

	if (!music->openFromFile("res/" + name + ".ogg")) return false;
	
	m_resources[name] = (void *)music;
	
	return true;
}

template<class T>
bool Resources::tryLoad(const std::string &name)
{
	std::cerr << "A attempt was made to load a resource for which no loader has been implemented yet!" << std::endl;
	return false;
}