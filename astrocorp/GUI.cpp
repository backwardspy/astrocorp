#include "GUI.h"

GUI::GUI() : m_active(true)
{

}

GUI::~GUI()
{

}

void GUI::input(const sf::Event &event)
{
	if (!m_active) return;
	for (auto control : m_controls) control->input(event);
}

void GUI::update(float dt)
{
	if (!m_active) return;
	for (auto control : m_controls) control->update(dt);
}

void GUI::draw(sf::RenderTarget &context)
{
	if (!m_active) return;
	for (auto control : m_controls) control->draw(context);
}