#include "Asteroid.h"
#include "MiningShip.h"
#include "Station.h"
#include "Resources.h"

MiningShip::MiningShip():
	Ship(2.0f, "sprites/ships/miner"),
	m_capacity(30), m_cargoAmount(0),
	m_timer(0.0f), m_digTimer(0.0f)
{
	auto *font = resources.get<sf::Font>("fonts/neuropol x free");

	if (font)
	{
		m_cargoText.setFont(*font);
		m_cargoText.setCharacterSize(16);
		m_cargoText.setColor(sf::Color::White);
	}
}

MiningShip::~MiningShip()
{

}

void MiningShip::update(float dt)
{
	Ship::update(dt);

	if (!m_asteroid) return;
	if (m_asteroid->ore <= 0)
	{
		m_asteroid = nullptr;
		return;
	}

	if (m_timer > 0.0f) m_timer -= dt;
	else
	{
		m_timer = Math::rand(30.0f, 60.0f);
		generateSitOffset();
	}

	if (m_digTimer > 0.0f) m_digTimer -= dt;

	if ((m_sitOffset - position).lengthSq() > 500.0f) flyTo(m_sitOffset);
	else
	{
		Vec2 diff = m_asteroid->position - position;
		float ang = atan2(diff.y, diff.x);
		rotate(ang);

		float angDiff = Math::wrapAngle(ang - rotation);

		if (m_cargoAmount < m_capacity)
		{
			if (abs(angDiff) <= 0.01f && m_digTimer <= 0.0f)
			{
				m_asteroid->ore--;
				m_cargoAmount++;
				m_digTimer = 0.5f;

				static const sf::Color colour1 = sf::Color(169, 105, 88);
				static const sf::Color colour2 = sf::Color(88, 75, 63);

				Vec2 orePos = m_asteroid->position + Math::randVec() * 24.0f;
				Vec2 diff = position - orePos;
				m_emitter.add(Particle(orePos, diff, colour1, colour2, 1.2f, Particle::GlowSmall));

				fireLaser(sf::Color::Cyan, 0.3f, 0.5f, diff.length());
			}
		}
		else if (m_dropOff)
		{
			flyTo(*m_dropOff, [&]{
				m_dropOff->ore += m_cargoAmount;
				m_cargoAmount = 0;
			});
		}
	}
	
	m_emitter.update(dt);
}

void MiningShip::draw(sf::RenderTarget &context)
{
	m_emitter.draw(context);

	Ship::draw(context);

	m_sstream << m_cargoAmount << "/" << m_capacity;
	m_cargoText.setString(m_sstream.str());
	m_cargoText.setPosition(position - Vec2(m_cargoText.getLocalBounds().width / 2.0f, 50.0f));
	context.draw(m_cargoText);
	m_sstream.str("");
}

void MiningShip::mine(Asteroid *asteroid)
{
	m_asteroid = asteroid;
	generateSitOffset();
}

void MiningShip::generateSitOffset()
{
	if (!m_asteroid) return;

	float radius = Math::rand(100.0f, 200.0f);
	float ang = Math::rand(Math::PI * 2.0f);

	m_sitOffset = m_asteroid->position + Vec2(cos(ang), sin(ang)) * radius;
}